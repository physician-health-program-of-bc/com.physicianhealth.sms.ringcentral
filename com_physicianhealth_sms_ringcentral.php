<?php
/**
 * In later versions of civi, Mapper requires this file, but I also need this
 * to work with older civi, so trying to do this in a way that works with
 * both and also seems cleaner.
 */
class com_physicianhealth_sms_ringcentral extends CRM_SMS_Provider_RingCentral {
    // Don't do anything here, just alias real file so that it works in both versions of civi.
}
