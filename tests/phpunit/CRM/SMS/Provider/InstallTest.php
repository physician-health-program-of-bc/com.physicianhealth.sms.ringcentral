<?php
/* vim: set shiftwidth=2 tabstop=2 softtabstop=2: */

use CRM_SMS_RingCentral_ExtensionUtil as E;
use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;

/**
 * InstallTest.
 *
 * Tips:
 *  - With HookInterface, you may implement CiviCRM hooks directly in the test class.
 *    Simply create corresponding functions (e.g. "hook_civicrm_post(...)" or similar).
 *  - With TransactionalInterface, any data changes made by setUp() or test****() functions will
 *    rollback automatically -- as long as you don't manipulate schema or truncate tables.
 *    If this test needs to manipulate schema or truncate tables, then either:
 *       a. Do all that using setupHeadless() and Civi\Test.
 *       b. Disable TransactionalInterface, and handle all setup/teardown yourself.
 *
 * @group headless
 */
class CRM_SMS_Provider_InstallTest extends \PHPUnit_Framework_TestCase implements HeadlessInterface, HookInterface, TransactionalInterface {

  public function setUpHeadless() {
    // Civi\Test has many helpers, like install(), uninstall(), sql(), and sqlFile().
    // See: https://docs.civicrm.org/dev/en/latest/testing/phpunit/#civitest
    return \Civi\Test::headless()
      ->installMe(__DIR__)
      ->apply();

    // Docs are unclear since it suggests you should be enabling ahead of time in your testing sandbox, but how does that work for headless? I think docs are outdated since it used to be that civix:test would pull the extension status from the actual dev site.
    // Also the installMe() call above I think already does this.
    $manager = \CRM_Extension_System::singleton()->getManager();
    if ($manager->getStatus('com.physicianhealth.sms.ringcentral') !== \CRM_Extension_Manager::STATUS_INSTALLED) {
      $manager->install(array('com.physicianhealth.sms.ringcentral'));
    }
  }

  /**
   * testInstall
   *
   * Check that it really did install by looking for some things.
   */
  public function testInstall() {
    $name = _ringcentral_determine_name();
    $optionID = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_OptionValue', $name, 'id', 'name');
    $this->assertNotEmpty($optionID);
  }

  public function testAddProvider() {
    $params = array(
      'name' => _ringcentral_determine_name(),
      'title' => 'Test RingCentral',
      'username' => '+12223334444',
      'password' => 'password',
      'api_type' => 1, //http
      'api_url' => 'https://test.test',
      'api_params' => "from=+12223334444\nextension=101\nclient_id=abc123\nclient_secret=def456",
      'is_active' => 1,
      'is_default' => 1,
    );

    $result = CRM_SMS_BAO_Provider::create($params);
    $this->assertNotEmpty($result->id);
  }

  public function testGetSingleton() {
    $this->testAddProvider();
    $params = array('provider' => _ringcentral_determine_name());
    $provider = CRM_SMS_Provider::singleton($params);
    $this->assertEquals($provider->_apiURL, 'https://platform.devtest.ringcentral.com');
  }
}
