# SMS Provider for RingCentral / Telus

## Overview
This extension provides SMS integration with RingCentral. It also works with Telus which rebrands RingCentral.

You can either use a single FROM number like most SMS providers, or there is also the option of having it use the logged-in user's direct phone number as the FROM number if you are using the virtual PBX features of RingCentral.

Legal notes: The Physician Health Program of British Columbia is not affiliated with RingCentral or Telus. This software is provided as-is.

## Installation/Configuration
1. Sign up with RingCentral. If you want to try it out first you can create a free test account at https://developers.ringcentral.com.
2. Create a Private (not public) app there that is for "Server Only (no UI)" and has SMS permission.
3. From the CiviCRM menu choose Administer - RingCentral. Choose whether to use a single FROM or a per-user FROM number.
   * If this doesn't appear in the menu, you may need to first visit [site url]/civicrm/menu/rebuild?reset=1.
   * The direct url is [site url]/civicrm/admin/sms/ringcentral?reset=1.
4. In CiviCRM add an SMS Provider for RingCentral. If you are using per-user FROM numbers the parameters here are the default/fallback if a given user does not have their own settings on their contact record (see below).
   * The username is one of:
      * The direct phone number assigned to a RingCentral user,  
         OR
      * The email address of a RingCentral user. (Note the user in RingCentral must have the checkbox to allow email login checked, and it must be verified unique by clicking the Verify Uniqueness button on their user account.)  
         OR
      * The main number for the account.
   * The api parameters are:
      * from: For this provider the from must match the direct phone number of the user in RingCentral, or the main number if no direct phone assigned.
      * extension: Optional unless the username is the main number.
      * client_id: Get this from the developer portal under the app under Credentials.
      * client_secret: Get this from the developer portal under the app under Credentials.
   * For the url, use https://platform.devtest.ringcentral.com for sandbox accounts and https://platform.ringcentral.com for live accounts.
5. To allow for the per-user settings this extension creates a custom field set that will appear on contact records who also have a CMS account. It does not have to be filled in and can be left blank to fall back to the settings defined in the SMS Provider record.

## Bulk SMS
This works with Bulk SMS but if you have chosen per-user FROM numbers then note that the "logged-in user" during the scheduled cron run will depend on how your cron is run and the user that is specified there, so it might fall back to the values used in the SMS Provider record.

## Other Notes
1. Phone numbers should be in E.164 format (e.g. +12223334444 for USA), although the '+' is optional.
2. Characters in the recipient phone number that aren't digits or a '+' are stripped out.
3. If you are using per-user FROM numbers and don't see the custom fields on a contact record, check that that contact actually has a user account in the CMS. The fields will only appear for actual users.
4. The passwords are stored unencrypted, and while per-user passwords are masked on view, anyone with edit access to the contact can see it. This is not too different from mail passwords for the email processor or bounce processing.
