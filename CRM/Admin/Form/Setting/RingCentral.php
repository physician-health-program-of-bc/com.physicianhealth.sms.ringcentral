<?php
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2: */

class CRM_Admin_Form_Setting_RingCentral extends CRM_Core_Form {
  const SETTINGS = 'com.physicianhealth.sms.ringcentral';
  const DOMAIN = 'com.physicianhealth.sms.ringcentral';

  function buildQuickForm() {
    CRM_Utils_System::setTitle(ts('RingCentral Settings', array('domain' => self::DOMAIN)));

    $this->addElement('select',
      'per_user_from',
      ts('Per-User FROM Number'),
      array(
        'N' => ts('No', array('domain' => self::DOMAIN)),
        'Y' => ts('Yes', array('domain' => self::DOMAIN)),
      )
    );

    // set defaults
    $defaults = array(
      'per_user_from' => _ringcentral_get_setting(self::SETTINGS, 'per_user_from'),
    );
    $this->setDefaults($defaults);

    $this->addButtons(array(
      array(
        'type' => 'submit',
        'name' => ts('Save', array('domain' => self::DOMAIN)),
        'isDefault' => TRUE,
      ),
    ));
    parent::buildQuickForm();
  }

  function postProcess() {
    parent::postProcess();
    $values = $this->exportValues();
    _ringcentral_set_setting($values['per_user_from'], self::SETTINGS, 'per_user_from');
    $statusMsg = ts('Your settings have been saved.', array('domain' => self::DOMAIN));
    CRM_Core_Session::setStatus( $statusMsg, '', 'success' );
  }
}
