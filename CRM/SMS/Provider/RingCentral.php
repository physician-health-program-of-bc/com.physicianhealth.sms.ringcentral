<?php
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2: */

/**
 * SMS Provider for RingCentral.
 * Also works for Telus which rebrands RingCentral.
 *
 * Partly copied and mixed and matched from Twilio and Clickatell.
 *
 * See README.md for the available api params you can set.
 */

class CRM_SMS_Provider_RingCentral extends CRM_SMS_Provider {

  protected $_apiType = 'http';

  protected $_providerInfo = array();

  public $_apiURL = "https://platform.devtest.ringcentral.com";

  /**
   * Token obtained after authentication
   */
  protected $_accessToken;

  static private $_singleton = array();

  /**
   * Curl handle resource id
   */
  protected $_ch;


  /**
   * Constructor
   */
  function __construct($provider = array(), $skipAuth = TRUE) {
    // Adjust for old civi versions which pass in numeric value.
    if (!empty($provider['api_type']) && is_numeric($provider['api_type'])) {
      $apiTypes = CRM_Core_OptionGroup::values('sms_api_type');
      if (!empty($apiTypes[$provider['api_type']])) {
        $provider['api_type'] = $apiTypes[$provider['api_type']];
      }
    }

    $this->_apiType = CRM_Utils_Array::value('api_type', $provider, 'http');
    $this->_providerInfo = $provider;

    // Initialize curl and some defaults
    if ($this->_apiType == 'http' && !empty($this->_providerInfo['username'])) {
      $this->_ch = curl_init();
      if (!$this->_ch || !is_resource($this->_ch)) {
        return PEAR::raiseError('Cannot initialize a new curl handle.');
      }
      /* For debugging, although doesn't show post data
      curl_setopt($this->_ch, CURLOPT_VERBOSE, 1);
      $fp = fopen('curlerr.txt', 'a');
      curl_setopt($this->_ch, CURLOPT_STDERR, $fp);
       */
      curl_setopt($this->_ch, CURLOPT_FAILONERROR, 1);
      if (ini_get('open_basedir') == '' && ini_get('safe_mode') == 'Off') {
        curl_setopt($this->_ch, CURLOPT_FOLLOWLOCATION, 1);
      }
      curl_setopt($this->_ch, CURLOPT_COOKIEJAR, "/dev/null");
      $verifySSL = _ringcentral_get_setting(CRM_Core_BAO_Setting::SYSTEM_PREFERENCES_NAME,
        'verifySSL'
      );
      // On windows verifying certs fails?
      if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        curl_setopt($this->_ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->_ch, CURLOPT_SSL_VERIFYPEER, 0);
      } else {
        curl_setopt($this->_ch, CURLOPT_SSL_VERIFYHOST, $verifySSL ? 2 : 0);
        curl_setopt($this->_ch, CURLOPT_SSL_VERIFYPEER, $verifySSL ? 1 : 0);
      }
      // return the result on success, FALSE on failure
      curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($this->_ch, CURLOPT_TIMEOUT, 3600);
      curl_setopt($this->_ch, CURLOPT_USERAGENT, 'CiviCRM');
    }

    if ($skipAuth) {
      return TRUE;
    }

    $this->authenticate();
  }

  /**
   * singleton function used to manage this object
   *
   * @return object
   * @static
   */
  static function &singleton($providerParams = array(), $force = FALSE) {
    $providerID = CRM_Utils_Array::value('provider_id', $providerParams);
    $skipAuth   = $providerID ? FALSE : TRUE;
    $cacheKey   = (int) $providerID;

    if (!isset(self::$_singleton[$cacheKey]) || $force) {
      $provider = array();
      if ($providerID) {
        $provider = CRM_SMS_BAO_Provider::getProviderInfo($providerID);
      }
      self::$_singleton[$cacheKey] = new CRM_SMS_Provider_RingCentral($provider, $skipAuth);
    }
    return self::$_singleton[$cacheKey];
  }

  /**
   * Authenticate using "password auth flow". The app in the developer console should be created as type "Server only No UI".
   */
  function authenticate() {
    curl_setopt($this->_ch, CURLOPT_URL, "{$this->_providerInfo['api_url']}/restapi/oauth/token");
    curl_setopt($this->_ch, CURLOPT_POST, TRUE);

    $basicAuth = base64_encode("{$this->_providerInfo['api_params']['client_id']}:{$this->_providerInfo['api_params']['client_secret']}");
    curl_setopt($this->_ch, CURLOPT_HTTPHEADER,
      array(
        'Content-Type: application/x-www-form-urlencoded',
        'Accept: application/json',
        "Authorization: Basic $basicAuth",
      )
    );

    $postValues = $this->getAuthValues();
    $postdata = 'username=' . urlencode($postValues['username'])
      . '&password=' . urlencode($postValues['password'])
      . '&grant_type=password';

    // extension is optional unless using the main number for login, i.e. username can be either the main number, the direct phone number for the user, or the email address. If using email address you need to click the "verify uniqueness" button in the user record at RingCentral and check the box on the user record to allow email for login.
    if (!empty($postValues['extension'])) {
      $postdata .= '&extension=' . urlencode($postValues['extension']);
    }
    curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $postdata);

    $response = curl_exec($this->_ch);
    if (empty($response)) {
      $errorMessage = 'Error: "' . curl_error($this->_ch) . '" - Code: ' . curl_errno($this->_ch);
      // This was copied from the original but seems awkward since it doesn't show up until the next page load, but leaving in since maybe it has some purpose. We include errorMessage below in the PEAR error so they can see it on the actual page where the error happens.
      CRM_Core_Session::setStatus($errorMessage, ts('SMS Error', array('domain' => CRM_Admin_Form_Setting_RingCentral::DOMAIN)), 'error');
    }
    $result = json_decode($response, TRUE);
    if (empty($result['access_token'])) {
      return PEAR::raiseError("Failed authentication. {$errorMessage}" . print_r($result, true));
    }
    $this->_accessToken = $result['access_token'];
    return TRUE;
  }

  /**
   * Send an SMS Message
   *
   * @param array the message with a to/from/text
   *
   * @return mixed SID on success or PEAR_Error object
   * @access public
   */
  function send($recipients, $header, $message, $jobID = NULL, $userID = NULL) {
    if ($this->_apiType == 'http' && !empty($this->_accessToken)) {
      $from = '';
      if (!empty($this->_providerInfo['api_params']['from'])) {
        $from = $this->_providerInfo['api_params']['from'];
      }

      // Possible override if using per-user FROM
      $authParams = $this->getAuthValues();
      if (!empty($authParams['from'])) {
        $from = $authParams['from'];
      }

      curl_setopt($this->_ch, CURLOPT_URL, "{$this->_providerInfo['api_url']}/restapi/v1.0/account/~/extension/~/sms");
      curl_setopt($this->_ch, CURLOPT_POST, TRUE);

      curl_setopt($this->_ch, CURLOPT_HTTPHEADER,
        array(
          'Content-Type: application/json',
          'Accept: application/json',
          "Authorization: Bearer {$this->_accessToken}",
        )
      );

      /**
       * Put recipients in desired format.
       *
       * This function seems to be called separately for each recipient regardless of whether it's a standalone sms with multiple recipients or a bulk sms sent via the process_sms job. So:
       * $recipients seems to be a string with a single phone each time through despite being named plurally. The other provider implementations don't seem to use it.
       * $header['To'] (with a capital T) seems to be the same as $recipients and seems to be what other providers use.
       * $header['to'] (with a small t) is blank for bulk SMS. When using the standalone SMS form it seems to be a comma-separated list of all the recipients, repeated each time the function is called for each recipient. Each comma-separated value is itself a '::'-separated value with contact_id::phone, e.g. 6816::222-333-4444,6817::222-455-6778
       */
      $recipientList = array(
        array(
          // remove everything except numbers and plus sign
          'phoneNumber' => preg_replace('/[^0-9\+]/', '', $header['To']),
        )
      );

      $postdata = array(
        'from' => array('phoneNumber' => $from),
        'to' => $recipientList,
        'text' => $message,
      );

      curl_setopt($this->_ch, CURLOPT_POSTFIELDS, json_encode($postdata));

      $response = curl_exec($this->_ch);
      if (empty($response)) {
        $errorMessage = 'Error: "' . curl_error($this->_ch) . '" - Code: ' . curl_errno($this->_ch);
        CRM_Core_Session::setStatus($errorMessage, ts('SMS Error', array('domain' => CRM_Admin_Form_Setting_RingCentral::DOMAIN)), 'error');
        return PEAR::raiseError($errorMessage);
      }

      if (PEAR::isError($response)) {
        return $response;
      }

      $result = json_decode($response, TRUE);

      if (!empty($result['errorCode'])) {
        return PEAR::raiseError($result['message'], $result['errorCode']);
      }

      $sid = $result['id'];
      // This seems awkward, since it creates a duplicate of the activity just with a different subject?
      // $this->createActivity($sid, $message, $header, $jobID, $userID);
      return $sid;
    }
  }

  /**
   * Not needed.
   */
  function callback() {
  	return TRUE;
  }

  /**
   * Inbound would probably work, but app would need read permission on
   * messages and I haven't looked into the api for it.
   */
  function inbound() {
    return PEAR::raiseError(ts('Inbound not currently supported.', array('domain' => CRM_Admin_Form_Setting_RingCentral::DOMAIN)));
  }

  /**
   * Helper function to assemble the authentication values.
   */
  protected function getAuthValues() {
    // Set org-wide defaults
    $defaults = array(
      'username' => $this->_providerInfo['username'],
      'password' => $this->_providerInfo['password'],
      'extension' => empty($this->_providerInfo['api_params']['extension']) ? NULL : $this->_providerInfo['api_params']['extension'],
    );

    /**
     * If using per-user FROM numbers, get the logged-in user's info.
     * Default to org defaults if blank/incomplete.
     *
     * During bulk SMS, this will end up attempting to use the contact that
     * the cron has been told to use (as opposed to the person who created
     * the bulk mailing).
     */
    $userValues = $this->getUserOverrideValues();

     // We don't want to merge the arrays, we want one or the other, and except for extension we need all the values to be present, or else fall back to defaults.
    if (empty($userValues['password']) || empty($userValues['username']) || empty($userValues['from'])) {
      return $defaults;
    }
    return $userValues;
  }

  /**
   * Get possible overrides from logged-in user's custom fields on their contact record.
   */
  protected function getUserOverrideValues() {
    $userValues = array();
    $per_user_from = _ringcentral_get_setting(CRM_Admin_Form_Setting_RingCentral::SETTINGS, 'per_user_from');
    if (!empty($per_user_from) && $per_user_from == 'Y') {
      $session = CRM_Core_Session::singleton();
      $user_id = $session->get('userID');
      if (!empty($user_id)) {
        // look up custom fields for this user
// TODO: Do this more better, but needs to work on old site too.
        require_once 'api/api.php';
        $fields = _ringcentral_custom_fields();
        foreach ($fields as $fieldParams) {
          $fid = _ringcentral_get_setting(CRM_Admin_Form_Setting_RingCentral::SETTINGS, "custom_field_{$fieldParams['name']}_id");
          if ($fid) {
            $params = array(
              'entity_id' => $user_id,
              "return.custom_{$fid}" => 1,
              'version' => 3,
            );
            $result = civicrm_api('custom_value', 'get', $params);
            if (empty($result['is_error'])) {
              if (empty($result['values'][$result['id']][0]) && $fieldParams['name'] != 'ringcentral_extension') {
                // They're all mandatory except extension, so if any of those aren't filled in then skip the whole thing.
                $userValues = array();
                break;
              } else {
                // get the part of the name that matches the POST parameter
                $pos = strpos($fieldParams['name'], '_');
                if ($pos !== FALSE) {
                  $userValues[substr($fieldParams['name'], $pos + 1)] = $result['values'][$result['id']][0];
                }
              }
            }
          }
        }
      }
    }
    return $userValues;
  }
}
