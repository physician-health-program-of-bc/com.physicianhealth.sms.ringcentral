<div class="crm-block crm-form-block crm-miscellaneous-form-block">

<h3>{ts domain='com.physicianhealth.sms.ringcentral'}RingCentral Settings{/ts}</h3>

<table class="form-layout">
  <tr>
    <td class="label">{$form.per_user_from.label}</td>
    <td class="content">{$form.per_user_from.html}
      <p class="description">{ts domain='com.physicianhealth.sms.ringcentral'}If set to Yes, the FROM phone number for outbound SMS will be specific to the logged in user. It will be taken from the RingCentral custom fields on the person's contact record. If blank there, it will fall back to the FROM defined in the SMS Provider settings.{/ts}</p>
    </td>
  </tr>
</table>

<div class="crm-submit-buttons">
{include file="CRM/common/formButtons.tpl" location="bottom"}
</div>

</div>
