<?php
/* vim: set tabstop=2 softtabstop=2 shiftwidth=2: */

require_once 'ringcentral.civix.php';

/**
 * Implementation of hook_civicrm_config
 */
function ringcentral_civicrm_config(&$config) {
  _ringcentral_civix_civicrm_config($config);
}

/**
 * Implementation of hook_civicrm_xmlMenu
 *
 * @param $files array(string)
 */
function ringcentral_civicrm_xmlMenu(&$files) {
  _ringcentral_civix_civicrm_xmlMenu($files);
}

/**
 * Implementation of hook_civicrm_install
 */
function ringcentral_civicrm_install() {
  $name = _ringcentral_determine_name();
  $groupID = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_OptionGroup', 'sms_provider_name', 'id', 'name');
  $params = array(
    'option_group_id' => $groupID,
    'label' => 'RingCentral',
    'value' => $name,
    'name'  => $name,
    'is_default' => 0,
    'is_active'  => 1,
    'version'    => 3,
  );
  require_once 'api/api.php';
  civicrm_api('option_value', 'create', $params);

  // Add custom group
  $params = array(
    'title' => 'RingCentral',
    'name'  => 'RingCentral',
    'extends' => 'Individual',
    'help_pre' => 'These fields are only used if the administrator has selected to use Per-User FROM Numbers in the RingCentral settings. If selected but left blank here, the defaults in the SMS Provider record will be used.',
    'collapse_display' => 1,
    'is_active'  => 1,
    'version'    => 3,
  );
  $result = civicrm_api('custom_group', 'create', $params);
  if (empty($result['is_error'])) {
    $custom_group_id = $result['id'];
    _ringcentral_set_setting($custom_group_id, _ringcentral_admin_form_setting_name(), 'ringcentral_custom_group_id');

    // Add custom fields
    $fields = _ringcentral_custom_fields();
    foreach ($fields as $fieldParams) {
      $fieldParams['custom_group_id'] = $custom_group_id;
      $result = civicrm_api('custom_field', 'create', $fieldParams);
      if (empty($result['is_error'])) {
        _ringcentral_set_setting($result['id'], _ringcentral_admin_form_setting_name(), "custom_field_{$fieldParams['name']}_id");
      } else {
        CRM_Core_Session::setStatus(ts('Unable to create custom field.', array('domain' => _ringcentral_admin_form_setting_name())), ts('SMS Error', array('domain' => _ringcentral_admin_form_setting_name())), 'error');
      }
    }
  } else {
    CRM_Core_Session::setStatus(ts('Unable to create custom group.', array('domain' => _ringcentral_admin_form_setting_name())), ts('SMS Error', array('domain' => _ringcentral_admin_form_setting_name())), 'error');
  }

  return _ringcentral_civix_civicrm_install();
}

/**
 * Implementation of hook_civicrm_uninstall
 */
function ringcentral_civicrm_uninstall() {
  $name = _ringcentral_determine_name();
  $optionID = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_OptionValue', $name, 'id', 'name');
  if ($optionID) {
    CRM_Core_BAO_OptionValue::del($optionID);
  }
  $filter =  array('name' => $name);
  $providers = CRM_SMS_BAO_Provider::getProviders(FALSE, $filter, FALSE);
  if ($providers) {
    foreach ($providers as $key => $value) {
      CRM_SMS_BAO_Provider::del($value['id']);
    }
  }

  // Remove custom fields
  require_once 'api/api.php';
  $fields = _ringcentral_custom_fields();
  foreach ($fields as $fieldParams) {
    $fid = _ringcentral_get_setting(_ringcentral_admin_form_setting_name(), "custom_field_{$fieldParams['name']}_id");
    if ($fid) {
      $params = array(
        'id' => $fid,
        'version' => 3,
      );
      civicrm_api('custom_field', 'delete', $params);
    }
  }

  // Remove custom group
  $gid = _ringcentral_get_setting(_ringcentral_admin_form_setting_name(), 'ringcentral_custom_group_id');
  if ($gid) {
    $params = array(
      'id' => $gid,
      'version' => 3,
    );
    civicrm_api('custom_group', 'delete', $params);
  }

  return _ringcentral_civix_civicrm_uninstall();
}

/**
 * Implementation of hook_civicrm_enable
 */
function ringcentral_civicrm_enable() {
  $name = _ringcentral_determine_name();
  $optionID = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_OptionValue', $name, 'id', 'name');
  if ($optionID) {
    CRM_Core_BAO_OptionValue::setIsActive($optionID, TRUE);
  }
  $filter = array('name' => $name);
  $providers = CRM_SMS_BAO_Provider::getProviders(FALSE, $filter, FALSE);
  if ($providers) {
    foreach ($providers as $key => $value) {
      CRM_SMS_BAO_Provider::setIsActive($value['id'], TRUE);
    }
  }

  $gid = _ringcentral_get_setting(_ringcentral_admin_form_setting_name(), 'ringcentral_custom_group_id');
  if ($gid) {
    require_once 'api/api.php';
    $params = array(
      'id' => $gid,
      'is_active' => 1,
      'version' => 3,
    );
    civicrm_api('custom_group', 'create', $params);
  }

  return _ringcentral_civix_civicrm_enable();
}

/**
 * Implementation of hook_civicrm_disable
 */
function ringcentral_civicrm_disable() {
  $name = _ringcentral_determine_name();
  $optionID = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_OptionValue', $name, 'id', 'name');
  if ($optionID) {
    CRM_Core_BAO_OptionValue::setIsActive($optionID, FALSE);
  }
  $filter = array('name' => $name);
  $providers = CRM_SMS_BAO_Provider::getProviders(FALSE, $filter, FALSE);
  if ($providers) {
    foreach ($providers as $key => $value) {
      CRM_SMS_BAO_Provider::setIsActive($value['id'], FALSE);
    }
  }

  $gid = _ringcentral_get_setting(_ringcentral_admin_form_setting_name(), 'ringcentral_custom_group_id');
  if ($gid) {
    require_once 'api/api.php';
    $params = array(
      'id' => $gid,
      'is_active' => 0,
      'version' => 3,
    );
    civicrm_api('custom_group', 'create', $params);
  }

  return _ringcentral_civix_civicrm_disable();
}

/**
 * Implementation of hook_civicrm_upgrade
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed  based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 */
function ringcentral_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _ringcentral_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implementation of hook_civicrm_managed
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 */
function ringcentral_civicrm_managed(&$entities) {
  return _ringcentral_civix_civicrm_managed($entities);
}

/**
 * Implementation of hook_civicrm_navigationMenu
 */
function ringcentral_civicrm_navigationMenu(&$menu) {
  _ringcentral_civix_insert_navigation_menu($menu, 'Administer', array(
    'label' => ts('RingCentral Settings', array('domain' => _ringcentral_admin_form_setting_name())),
    'name' => 'ringcentral_settings',
    'permission' => 'administer CiviCRM',
    'child' => array(),
    'operator' => 'AND',
    'separator' => 0,
    'url' => CRM_Utils_System::url('civicrm/admin/sms/ringcentral', 'reset=1', TRUE),
  ));
}

/**
 * Implementation of hook_civicrm_summary
 * @see _ringcentral_isHideCustom()
 */
function ringcentral_civicrm_summary($contactID, &$content, &$contentPlacement = CRM_Utils_Hook::SUMMARY_BELOW) {
  if (_ringcentral_isHideCustom($contactID)) {
    CRM_Core_Resources::singleton()->addStyle('div#customFields div.RingCentral { display: none; }');
  }
  // in all cases mask the password
  // hmm.. no good selector I can see... and if someone rearranges the custom field order in the UI this will fail. But it's not meant as security it's just so it's not blatantly displayed.
  CRM_Core_Resources::singleton()->addScript('(function($){ $("div#customFields div.RingCentral div.crm-custom-data:last").text("*****"); })(jQuery)');
}

/**
 * Implementation of hook_civicrm_buildForm
 * @see _ringcentral_isHideCustom()
 */
function ringcentral_civicrm_buildForm($formName, &$form) {
  if ($formName == 'CRM_Contact_Form_Contact') {
    if (_ringcentral_isHideCustom($form->_contactId)) {
      $gid = _ringcentral_get_setting(_ringcentral_admin_form_setting_name(), 'ringcentral_custom_group_id');
      if ($gid) {
        CRM_Core_Resources::singleton()->addScript('(function($){ $("div#customData' . $gid . '").parent().hide(); })(jQuery)');
      }
    } else {
      // If not the same person as logged in user, hide the fields.
      // I know it's not real security, just less in-your-face.
      $session = CRM_Core_Session::singleton();
      $gid = _ringcentral_get_setting(_ringcentral_admin_form_setting_name(), 'ringcentral_custom_group_id');
      if ($gid) {
        $uid = $session->get('userID');
        if (!$uid || ($session->get('userID') != $form->_contactId)) {
          CRM_Core_Resources::singleton()->addScript('(function($){ $("div#customData' . $gid . '").parent().hide(); })(jQuery)');
        }
      }
    }
  }
}

/**
 * Helper for hook_summary and hook_buildForm
 * If contact being viewed does not have a CMS login, or per_user_from is not enabled in settings, then hide the custom fields.
 *
 * @param int $contactID The contact being viewed.
 *
 * @return bool Should the custom fields be hidden.
 */
function _ringcentral_isHideCustom($contactID) {
  $hideit = FALSE;
  if (!empty($contactID)) {
    $uid = CRM_Core_BAO_UFMatch::getUFId($contactID);
  }
  if (empty($uid)) {
    // doesn't have a matching CMS account
    $hideit = TRUE;
  }
  if (!$hideit) {
    $per_user_from = _ringcentral_get_setting(_ringcentral_admin_form_setting_name(), 'per_user_from');
    if (empty($per_user_from) || $per_user_from != 'Y') {
      $hideit = TRUE;
    }
  }
  return $hideit;
}

/**
 * Determine what to use based on which naming convention is in use.
 * In later versions it forces you to use a name with dots in it and then
 * have a file based on that name in the root of the extension. In earlier
 * versions it requires it to be the last part of the class name and
 * auto-prefixes it with "CRM_SMS_Provider_".
 */
function _ringcentral_determine_name() {
  $version = CRM_Core_BAO_Domain::version(true);
  if (version_compare($version, '4.3') < 0) {
    return 'RingCentral';
  } else {
    return 'com.physicianhealth.sms.ringcentral';
  }
}

/**
 * Since the file CRM_Admin_Form_Setting_RingCentral isn't available in this
 * context, just duplicate this setting.
 */
function _ringcentral_admin_form_setting_name() {
  return 'com.physicianhealth.sms.ringcentral';
}

/**
 * Helper to list the custom fields and their parameters.
 * We can't put this in a BAO or something because it's needed
 * during install, so the extension code files aren't even available yet.
 *
 * Note the naming convention for "name" - the second part matches the values
 * used in POST requests later.
 */
function _ringcentral_custom_fields() {
  return array(
    array(
      'label' => 'Direct Phone',
      'name' => 'ringcentral_from',
      'help_pre' => "Enter the phone in E.164 format, e.g. +12223334444 for USA/Canada. This is the FROM number that will be used, but it can't be arbitrary it has to match up to how you are set up in RingCentral, either the main number if you have no direct phone, or the direct phone that matches your extension.",
      'data_type' => 'String',
      'html_type' => 'Text',
      'text_length' => 255,
      'weight' => 1,
      'is_active'  => 1,
      'version'    => 3,
    ),
    array(
      'label' => 'Extension',
      'name' => 'ringcentral_extension',
      'help_pre' => 'Your phone extension. Optional unless you have no direct phone and are using the main number above. If you have a direct phone, you can omit this, but if you provide it then it must be the same one that is assigned to your direct phone. Example: 102',
      'data_type' => 'Int',
      'html_type' => 'Text',
      'weight' => 2,
      'is_active'  => 1,
      'version'    => 3,
    ),
    array(
      'label' => 'Username',
      'name' => 'ringcentral_username',
      'help_pre' => 'Either direct phone, email, or main phone, depending on your setup.',
      'data_type' => 'String',
      'html_type' => 'Text',
      'text_length' => 255,
      'weight' => 3,
      'is_active'  => 1,
      'version'    => 3,
    ),
    array(
      'label' => 'Password',
      'name' => 'ringcentral_password',
      'help_pre' => 'The password you use to log into RingCentral.',
      'data_type' => 'String',
      'html_type' => 'Text',
      'text_length' => 255,
      'weight' => 4,
      'is_active'  => 1,
      'version'    => 3,
    ),
  );
}

/**
 * Again this is used during install so we need it to be available.
 *
 * The BAO functions are deprecated in 5.21 but want to support older
 * versions too that don't have the 5.x functions.
 */
function _ringcentral_get_setting($groupName, $settingName) {
  $version = CRM_Core_BAO_Domain::version();
  if (version_compare($version, '5.21') < 0) {
    return CRM_Core_BAO_Setting::getItem($groupName, $settingName);
  }
  return \Civi::settings()->get($settingName);
}

function _ringcentral_set_setting($value, $groupName, $settingName) {
  $version = CRM_Core_BAO_Domain::version();
  if (version_compare($version, '5.21') < 0) {
    CRM_Core_BAO_Setting::setItem($value, $groupName, $settingName);
  } else {
    \Civi::settings()->set($settingName, $value);
  }
}
